package nanotech.lib.Item;

import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

/*
 * Created by viridior on 6/18/2016.
 */

public class ItemNanobots extends Item {

    public ItemNanobots() {

        setRegistryName("itemNanobots");
        setUnlocalizedName("itemNanobots");
        setCreativeTab(CreativeTabs.MISC);
        GameRegistry.register(this);

    }

    @SideOnly(Side.CLIENT)
    public void initModel() {

        ModelLoader.setCustomModelResourceLocation(this, 0, new ModelResourceLocation(getRegistryName(), "inventory"));

    }

}
