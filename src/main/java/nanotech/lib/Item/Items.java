package nanotech.lib.Item;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

/*
 * Created by viridior on 6/18/2016.
 */

public class Items {

    public static ItemNanobots itemNanobots;

    public static void init() {

        itemNanobots = new ItemNanobots();
        itemNanobots.setUnlocalizedName("itemNanobots");
        itemNanobots.setCreativeTab(CreativeTabs.MISC);
        GameRegistry.register(itemNanobots);

    }

    @SideOnly(Side.CLIENT)
    public static void initModels() {

        //place items that require model rendering here...
        itemNanobots.initModel();

    }
}
