package nanotech.lib.Proxy;

import nanotech.lib.Item.Items;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

/*
 * Created by viridior on 6/18/2016.
 */

public class ClientProxy extends CommonProxy {

    public void preInit(FMLPreInitializationEvent event) {

        super.preInit(event);

        //Initialize Custom Models
        Items.initModels();

    }

    public void init(FMLInitializationEvent event) {

        super.init(event);

    }

    public void postInit(FMLPostInitializationEvent event) {

        super.postInit(event);

    }

}
