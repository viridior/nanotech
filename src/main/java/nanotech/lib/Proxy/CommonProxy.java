package nanotech.lib.Proxy;

import nanotech.lib.Item.Items;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

/*
 * Created by viridior on 6/18/2016.
 */

public class CommonProxy {

    public void preInit(FMLPreInitializationEvent event) {

        Items.init();

    }

    public void init(FMLInitializationEvent event) {

    }

    public void postInit(FMLPostInitializationEvent event) {

    }

}