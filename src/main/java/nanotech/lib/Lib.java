package nanotech.lib;

import nanotech.lib.Proxy.CommonProxy;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import org.apache.logging.log4j.Logger;

import static mcp.mobius.waila.addons.thaumcraft.ThaumcraftModule.CommonProxy;

/*
 * Created by viridior on 6/18/2016.
 * [References]
 * http://bedrockminer.jimdo.com/modding-tutorials/
 * https://github.com/McJty/ModTutorials/tree/1.9.4
 *
 */

@Mod(modid = Lib.MODID, name = Lib.MODNAME, version = Lib.VERSION, dependencies = "required-after:Forge@[12.17.0.1965,)", useMetadata = true)
public class Lib {

    public static final String MODID = "nanotech.lib";
    public static final String MODNAME = "Nanotech (Library)";
    public static final String VERSION = "0.1";

    @SidedProxy(clientSide="nanotech.lib.Proxy.ClientProxy", serverSide="nanotech.lib.Proxy.CommonProxy")
    public static CommonProxy proxy;

    @Mod.Instance
    public static Lib instance;

    public static Logger logger;

    @EventHandler
    public void preInit(FMLPreInitializationEvent event) {

        logger = event.getModLog();
        proxy.preInit(event);

    }

    @EventHandler
    public void init(FMLInitializationEvent event) {
        proxy.init(event);
    }

    @EventHandler
    public void postInit(FMLPostInitializationEvent event) {
        proxy.postInit(event);
    }

}